﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Solomon.LanguageTranslation._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <asp:Panel ID="Panel0" runat="server" Visible="false">
        <div class="form-horizontal" runat="server">
            <hr />
            <p class="text-warning">
                <asp:Literal runat="server" ID="Literal1">No Document for translation was uploaded! </asp:Literal><asp:HyperLink NavigateUrl="/Account/Documents" Text="Click here to upload one." ID="HyperLink1" runat="server" />
            </p>
        </div>
    </asp:Panel>
    <asp:Panel ID="Panel1" runat="server" Visible="false">
        <div class="form-horizontal" runat="server">
            <hr />
            <p class="text-warning">
                <asp:Literal runat="server" ID="InfoMessage">No User Translation Languages configured! </asp:Literal><asp:HyperLink NavigateUrl="/Account/UserLanguages" Text="Click here to configure them." ID="RegisterUser" runat="server" />
            </p>
        </div>
    </asp:Panel>
    <asp:Panel ID="Panel2" runat="server" Visible="false">
        <h4>Available Documents</h4>
        <hr />
        <asp:GridView ID="GridView1" runat="server" CssClass="table table-striped table-bordered table-condensed" OnRowCommand="GridView1_RowCommand">
            <Columns>
               <asp:TemplateField HeaderText="Document Name">
                    <ItemTemplate>
                        <asp:LinkButton ID="DocumentID" runat="server"
                            Text='<%# Eval("DocumentName") %>'
                            CommandName="SetDocumentID"
                            CommandArgument='<%#Bind("DocumentID") %>'>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="UserName" HeaderText="Created By" />
            </Columns>
        </asp:GridView>
    </asp:Panel>
    
</asp:Content>

﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using Microsoft.AspNet.Identity;

namespace Solomon.LanguageTranslation
{
    public partial class _Default : Page
    {
        #region private variables
        string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString.ToString();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!((HttpContext.Current.User != null) && HttpContext.Current.User.Identity.IsAuthenticated))
            {
                Response.Redirect("/Account/Login");
            }

            if (!IsPostBack)
            {
                var translatorId = GetUserTranslatorId(User.Identity.GetUserId());
                var hasDocError = false;
                var hasLangError = false;

                var SqlConnection1 = new SqlConnection();
                SqlConnection1.ConnectionString = _connectionString;
                SqlCommand cmd = new SqlCommand("select * from Document", SqlConnection1);
                cmd.Connection.Open();
                SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                hasDocError = !(reader.HasRows);
                cmd.Connection.Close();

                var SqlConnection2 = new SqlConnection();
                SqlConnection2.ConnectionString = _connectionString;
                SqlCommand cmd1 = new SqlCommand("select l.LanguageName, l.LanguageAbbr  from TranslatorAccess as ta inner join Languages as l on ta.LanguageID = l.LanguageID where ta.TranslatorID = @translatorID", SqlConnection2);
                cmd1.Parameters.AddWithValue("@translatorID", translatorId);
                cmd1.Connection.Open();
                SqlDataReader reader1 = cmd1.ExecuteReader(CommandBehavior.CloseConnection);
                hasLangError = !(reader1.HasRows);
                cmd1.Connection.Close();

                if (hasDocError || hasLangError)
                {
                    Panel2.Visible = false;
                    Panel1.Visible = hasLangError;
                    Panel0.Visible = hasDocError;
                }else
                {
                    Panel2.Visible = true;
                }
                
                var SqlCon1 = new SqlConnection(_connectionString);
                DataSet ds = new DataSet();
                SqlDataAdapter ad = new SqlDataAdapter("Select d.DocumentID, d.DocumentName, t.UserName from  dbo.Document as d inner join Translator as t on d.CreatedBy = t.TranslatorID", SqlCon1);
                ad.Fill(ds);
                GridView1.AutoGenerateColumns = false;
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        GridView1.DataSource = ds;
                        GridView1.DataBind();
                    }
                    else
                    {
                        GridView1.DataSource = null;
                        GridView1.DataBind();
                    }
                }
            }
        }

        public int GetUserTranslatorId(string userId)
        {
            try
            {
                int translatrId = 0;
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SELECT TranslatorID FROM Translator WHERE UserID = @UserId", conn))
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Add("UserId", SqlDbType.VarChar).Value = userId;
                        translatrId = (Int32)cmd.ExecuteScalar();
                        conn.Close();
                    }
                }
                return translatrId;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "SetDocumentID")
            {
                Session["TargetDocument"] = e.CommandArgument.ToString();
                Response.Redirect("/Translation");
            }
        }
    }

}
﻿<%@ Page Title="Translate" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Translation.aspx.cs" Inherits="Solomon.LanguageTranslation.Translation" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="form-horizontal" runat="server">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Translate</li>
        </ol>

        <hr />
        <p class="text-info">
            <asp:Literal runat="server" ID="InfoMessage" />
        </p>
        <p class="text-danger">
            <asp:Literal runat="server" ID="ErrorMessage" />
        </p>
        <p class="text-success">
            <asp:Literal runat="server" ID="SuccessMessage" />
        </p>

        <div class="form-group">
            <div class="form-row">
                <div class="col-md-6">
                    <asp:Label runat="server" AssociatedControlID="SelectDocument">Document</asp:Label>
                    <asp:DropDownList ID="SelectDocument" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="SelectDocument_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div class="col-md-6">
                    <asp:Label runat="server" AssociatedControlID="SelectLanguage" CssClass="control-label">Translation Language</asp:Label>
                    <asp:DropDownList ID="SelectLanguage" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="SelectLanguage_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="form-row">
                <div class="col-md-6">
                </div>
                <div class="col-md-6">
                    <div class="pull-right">
                        <asp:Button ID="SaveTranslation" runat="server" CssClass="btn btn-default" Text="Save Translation" OnClick="SaveTranslation_Click" />
                        <asp:Button ID="GenerateXML" runat="server" CssClass="btn btn-primary" Text="Generate XML" OnClientClick="openInNewTab();" OnClick="GenerateXML_Click" />
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="form-row">
                <div class="col-md-12">
                    <asp:GridView ID="GridView1" CssClass="table table-striped table-bordered table-condensed translation-grid" runat="server">
                        <%--PageSize="10" AllowPaging="true"--%>
                        <Columns>
                            <asp:BoundField DataField="TagName" HeaderText="Tag Name" ItemStyle-Width="33%" />
                            <asp:BoundField DataField="TagText" HeaderText="English Text" ItemStyle-Wrap="true" ItemStyle-Width="33%" />
                            <asp:TemplateField HeaderText="Translation" ItemStyle-Width="33%">
                                <ItemTemplate>
                                    <asp:TextBox ID="TranslatedStr" runat="server" ReadOnly="true" CssClass="form-control" Text='<%# Eval("TranslatedText") %>'></asp:TextBox>
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("IsAttribute") %>' />
                                    <asp:HiddenField ID="HiddenField2" runat="server" Value='<%# Bind("TranslationID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>

    </div>
    <script type="text/javascript">
        function openInNewTab() {
            window.document.forms[0].target = '_blank';
            setTimeout(function () { window.document.forms[0].target = ''; }, 0);
        }
    </script>
</asp:Content>

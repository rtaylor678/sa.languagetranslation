﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;

namespace Solomon.LanguageTranslation
{
    public partial class SiteMaster : MasterPage
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;

        protected void Page_Init(object sender, EventArgs e)
        {
            //Display layout conditional
            if (!((HttpContext.Current.User != null) && HttpContext.Current.User.Identity.IsAuthenticated))
            {
                pagetop.Attributes["class"] = "fixed-nav";
                stickyF.Attributes["class"] = "sticky-footerLogin";
                ContentW.Attributes["class"] = "content-wrapper content-wrapperLogin";
            }
            else
            {
                pagetop.Attributes["class"] = "fixed-nav sticky-footer bg-dark";
                stickyF.Attributes["class"] = "sticky-footer";
                ContentW.Attributes["class"] = "content-wrapper";
            }

            // The code below helps to protect against XSRF attacks
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;
            if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                // Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value;
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            else
            {
                // Generate a new Anti-XSRF token and save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    HttpOnly = true,
                    Value = _antiXsrfTokenValue
                };
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                {
                    responseCookie.Secure = true;
                }
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //roles and authorization
            //Determine what controls to load
            if (HttpContext.Current.User.IsInRole("SuperAdmin") || HttpContext.Current.User.IsInRole("Admin"))
            {
                HyperLink hl1 = (HyperLink)LoginView1.FindControl("RegisterUser");
                if(hl1 != null)
                {
                    hl1.Visible = true;
                }
                HyperLink hl2 = (HyperLink)LoginView1.FindControl("DocumentUpload");
                if (hl2 != null)
                {
                    hl2.Visible = true;
                }
                HyperLink hl3 = (HyperLink)LoginView1.FindControl("Languages");
                if (hl3 != null)
                {
                    hl3.Visible = true;
                }
                HyperLink hl4 = (HyperLink)LoginView1.FindControl("ManageUser");
                if (hl4 != null)
                {
                    hl4.Visible = true;
                }
            }
        }

        protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Context.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

    }

}
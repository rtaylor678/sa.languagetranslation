﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Solomon.LanguageTranslation
{
    public partial class Translation : System.Web.UI.Page
    {
        #region private variables
        string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString.ToString();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!((HttpContext.Current.User != null) && HttpContext.Current.User.Identity.IsAuthenticated))
            {
                Response.Redirect("/Account/Login");
            }

            if (!IsPostBack)
            {
                if (Session["TargetDocument"] != null && !String.IsNullOrEmpty(Session["TargetDocument"].ToString()))
                {

                    InfoMessage.Text = "Please select a translation language to begin translation.";
                    //Read value from session
                    string targetDocumentID = Session["TargetDocument"].ToString();

                    SqlConnection conn = new SqlConnection(_connectionString);
                    DataSet ds = new DataSet();
                    SqlDataAdapter myda = new SqlDataAdapter("select DocumentID, DocumentName from Document", conn);
                    myda.Fill(ds);
                    SelectDocument.DataSource = ds;
                    SelectDocument.DataMember = ds.Tables[0].TableName;
                    SelectDocument.DataValueField = ds.Tables[0].Columns[0].ColumnName;
                    SelectDocument.DataTextField = ds.Tables[0].Columns[1].ColumnName;
                    SelectDocument.DataBind();
                    SelectDocument.Items.FindByValue(targetDocumentID).Selected = true;

                    DataSet ds1 = new DataSet();
                    SqlCommand cmd = new SqlCommand("spGetTranslatorLang", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserID", User.Identity.GetUserId());
                    SqlDataAdapter myda1 = new SqlDataAdapter(cmd);
                    myda1.Fill(ds1);
                    SelectLanguage.DataSource = ds1;
                    SelectLanguage.DataMember = ds1.Tables[0].TableName;
                    SelectLanguage.DataValueField = ds1.Tables[0].Columns[0].ColumnName;
                    SelectLanguage.DataTextField = ds1.Tables[0].Columns[1].ColumnName;
                    SelectLanguage.DataBind();
                    SelectLanguage.Items.Insert(0, "- Select Language -");

                    conn.Close();

                    LoadGrid();
                }
                else
                {
                    Response.Redirect("/Default");
                }
            }
        }

        private int GetUserTranslatorId(string userId)
        {
            try
            {
                int translatrId = 0;
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SELECT TranslatorID FROM Translator WHERE UserID = @UserId", conn))
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Add("UserId", SqlDbType.VarChar).Value = userId;
                        translatrId = (Int32)cmd.ExecuteScalar();
                        conn.Close();
                    }
                }
                return translatrId;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void LoadGrid()
        {
            var targetLang = (Session["TargetLanguage"] != null && !String.IsNullOrEmpty(Session["TargetLanguage"].ToString())) ? Session["TargetLanguage"].ToString() : "1";
            var langSelected = (Session["TargetLanguage"] != null && !String.IsNullOrEmpty(Session["TargetLanguage"].ToString())) ? 1 : 0;
            SqlConnection conn2 = new SqlConnection(_connectionString);

            SqlCommand cmd1 = new SqlCommand("spGetAllTagsByLang", conn2);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.Parameters.Clear();
            cmd1.Parameters.AddWithValue("@DocumentID", Convert.ToInt32(Session["TargetDocument"].ToString()));
            cmd1.Parameters.AddWithValue("@LanguageID", Convert.ToInt32(targetLang));
            cmd1.Parameters.AddWithValue("@LanguageSelected", langSelected);

            DataSet ds2 = new DataSet();
            SqlDataAdapter dataAdapter;
            try
            {
                if (conn2.State.Equals(ConnectionState.Closed))
                {
                    conn2.Open();
                    dataAdapter = new SqlDataAdapter(cmd1);
                    dataAdapter.Fill(ds2);
                    GridView1.AutoGenerateColumns = false;
                    conn2.Close();
                    if (ds2 != null)
                    {
                        if (ds2.Tables[0].Rows.Count != 0)
                        {
                            GridView1.DataSource = ds2;
                            GridView1.DataBind();
                        }
                        else
                        {
                            GridView1.DataSource = null;
                            GridView1.DataBind();
                        }
                    }
                }
            }
            catch (Exception err)
            {
                ErrorMessage.Text = err.Message.ToString();
            }
        }

        protected void SelectDocument_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddown = (DropDownList)sender;
            string selected = ddown.SelectedValue;
            Session["TargetDocument"] = selected;
            Response.Redirect(Request.RawUrl);
        }

        protected void SelectLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["TargetLanguage"] = (SelectLanguage.SelectedIndex != 0) ? SelectLanguage.SelectedValue: null;
            LoadGrid();
            if (SelectLanguage.SelectedIndex != 0)
            {
                InfoMessage.Text = "";
                foreach (GridViewRow gv in GridView1.Rows)
                {
                    ((TextBox)gv.FindControl("TranslatedStr")).ReadOnly = false;
                }
            }
            else
            {
                foreach (GridViewRow gv in GridView1.Rows)
                {
                    ((TextBox)gv.FindControl("TranslatedStr")).ReadOnly = true;
                }
                InfoMessage.Text = "Please select a translation language to begin translation.";
            }
        }

        protected void SaveTranslation_Click(object sender, EventArgs e)
        {
            var list = new List<string>();
            var gridrows = new List<GridViewRow>();
            foreach (GridViewRow gv in GridView1.Rows)
            {
                string translated = ((TextBox)gv.FindControl("TranslatedStr")).Text;
                if (!string.IsNullOrEmpty(translated))
                {
                    gridrows.Add(gv);
                    list.Add(translated);
                }
            }
            if (list.Any())
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("spCRUDTransTagsByLang", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        conn.Open();
                        foreach (var row in gridrows)
                        {

                            var isAtt = ((HiddenField)row.FindControl("HiddenField1")).Value;
                            string translated = ((TextBox)row.FindControl("TranslatedStr")).Text;
                            var tagname = row.Cells[0].Text;
                            string targetDocumentID = Session["TargetDocument"].ToString();
                            var laguageID = SelectLanguage.SelectedItem.Value;
                            var translatorId = GetUserTranslatorId(User.Identity.GetUserId());

                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@DocumentID", targetDocumentID);
                            cmd.Parameters.AddWithValue("@LanguageID", laguageID);
                            cmd.Parameters.AddWithValue("@TranslatorID", translatorId);
                            cmd.Parameters.AddWithValue("@TagName", tagname);
                            cmd.Parameters.AddWithValue("@TagText", translated);
                            cmd.Parameters.AddWithValue("@IsAttribute", isAtt);
                            cmd.Parameters.AddWithValue("@Active", 1);
                            cmd.ExecuteNonQuery();
                        }
                        conn.Close();
                    }
                }
                SuccessMessage.Text = "Translated Tags saved successfully!";
            }
            else
            {
                ErrorMessage.Text = "There are no translations to save!";
            }
        }

        protected void GenerateXML_Click(object sender, EventArgs e)
        {
            ////start building xml file
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<?xml version=\"1.0\"" + " encoding=\"utf-8\"" + " ?>");
            sb.AppendLine("<localizationDictionary culture=\"en\"" + ">");
            sb.AppendLine("  <texts>");

            foreach (GridViewRow gv in GridView1.Rows)
            {
                string isAtt = ((HiddenField)gv.FindControl("HiddenField1")).Value;
                string translated = ((TextBox)gv.FindControl("TranslatedStr")).Text;
                translated = EscapeXML(translated);
                if (isAtt == "True")
                {
                    sb.AppendFormat("<text name=\"{0}\"" + " value =\"{1}\"" + " />", gv.Cells[0].Text, translated);
                    sb.Append(Environment.NewLine);
                }
                else
                {
                    sb.AppendFormat("<text name=\"{0}\"" + ">{1}</text>", gv.Cells[0].Text, translated);
                    sb.Append(Environment.NewLine);
                }
            }
            sb.AppendLine("</texts>");
            sb.AppendLine("</localizationDictionary >");

            var final = sb.ToString();
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(final);

            // Load some xml 
            MemoryStream ms = new MemoryStream();
            using (XmlWriter writer = XmlWriter.Create(ms))
            {
                xdoc.WriteTo(writer); // Write to memorystream
            }

            byte[] data = ms.ToArray();
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "text/xml";
            HttpContext.Current.Response.Headers.Add("Content-Disposition:", "attachment;filename=" + HttpUtility.UrlEncode("TranslatedDocument.xml")); // Replace with name here
            HttpContext.Current.Response.BinaryWrite(data);
            HttpContext.Current.Response.End();
            ms.Close();
        }

        private string EscapeXML(string text)
        {
            return SecurityElement.Escape(text);
        }
    }
}
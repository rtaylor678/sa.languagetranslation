﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Solomon.LanguageTranslation.Account
{
    public partial class ManageUsers : System.Web.UI.Page
    {
        #region private variables
        string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString.ToString();
        private static string _currentRecord;
        protected string SuccessMessage
        {
            get;
            private set;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!((HttpContext.Current.User != null) && HttpContext.Current.User.Identity.IsAuthenticated))
            {
                Response.Redirect("/Account/Login");
            }

            if (!IsPostBack)
            {
                var SqlCon1 = new SqlConnection(_connectionString);
                DataSet ds = new DataSet();

                SqlCommand cmd = new SqlCommand("select u.Id, u.UserName, t.FirstName, t.LastName, u.Email, r.Name as Role from users as u inner join UserRoles as ur on u.Id = ur.UserId inner join Roles as r on r.Id = ur.RoleId inner join Translator as t on t.UserID = u.Id Where r.Name <> 'SuperAdmin' and u.id <> @currentUserId", SqlCon1);
                cmd.Parameters.Add("currentUserId", SqlDbType.VarChar).Value = User.Identity.GetUserId();
                SqlDataAdapter ad = new SqlDataAdapter(cmd);

                ad.Fill(ds);
                GridView1.AutoGenerateColumns = false;
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        GridView1.DataSource = ds;
                        GridView1.DataBind();
                    }
                    else
                    {
                        GridView1.DataSource = null;
                        GridView1.DataBind();
                    }
                }
                var message = Request.QueryString["m"];
                if (message != null)
                {
                    SuccessMessage =
                        message == "ResetPwdSuccess" ? "User Password reset successful."
                        : String.Empty;
                    PlaceHolder1.Visible = !String.IsNullOrEmpty(SuccessMessage);
                }
            }

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName != "ResetUser") return;
            _currentRecord = e.CommandArgument.ToString();
            lblModalTitle.Text = "Reset Password";
            lblModalBody.Text = "Are you sure you want to reset the login credentials for this user? An email with temporary credentials will be sent to this user.";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
            upModal.Update();
        }

        protected void reset_ServerClick(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            string resetToken = manager.GeneratePasswordResetToken(_currentRecord);
            string password = Membership.GeneratePassword(12, 1);
            var result = manager.ResetPassword(_currentRecord, resetToken, password);
            if (result.Succeeded)
            {
                //string message = "Below are your temporary Login Credentials for Solomon Language Translator application.<br /><br /><b> UserName: " + User.Identity.GetUserName() + "</b><br /><b> Password: " + password + "</b><br /> <br /><br /> Please make sure you change your login password after the first login.";
                //manager.SendEmail(_currentRecord, "Solomon Language Translator Login Credentials", message);
                Response.Redirect("~/Account/ManageUsers?m=ResetPwdSuccess");
            }
            ErrorMessage.Text = result.Errors.FirstOrDefault();
            return;
        }
    }
}
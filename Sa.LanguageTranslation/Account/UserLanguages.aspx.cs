﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Solomon.LanguageTranslation.Account
{
    public partial class UserLanguages : System.Web.UI.Page
    {
        #region private variables
        string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString.ToString();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!((HttpContext.Current.User != null) && HttpContext.Current.User.Identity.IsAuthenticated))
            {
                Response.Redirect("/Account/Login");
            }
            if (!IsPostBack)
            {
                SqlConnection conn = new SqlConnection(_connectionString);

                DataSet ds = new DataSet();
                SqlDataAdapter myda = new SqlDataAdapter("select LanguageID, LanguageName from Languages ", conn);
                myda.Fill(ds);
                Ddlanguage.DataSource = ds;
                Ddlanguage.DataMember = ds.Tables[0].TableName;
                Ddlanguage.DataValueField = ds.Tables[0].Columns[0].ColumnName;
                Ddlanguage.DataTextField = ds.Tables[0].Columns[1].ColumnName;
                Ddlanguage.DataBind();
                Ddlanguage.Items.Insert(0, "- Select Language -");

                var translatorId = GetUserTranslatorId(User.Identity.GetUserId());

                var cmdString = ("select l.LanguageName, l.LanguageAbbr  from TranslatorAccess as ta inner join Languages as l on ta.LanguageID = l.LanguageID where ta.TranslatorID = @TranslatorId");
                SqlCommand cmd = new SqlCommand(cmdString, conn);
                cmd.Parameters.Add("TranslatorId", SqlDbType.Int).Value = translatorId;
                DataSet ds1 = new DataSet();
                SqlDataAdapter dataAdapter;
                try
                {
                    if (conn.State.Equals(ConnectionState.Closed))
                    {
                        conn.Open();
                        dataAdapter = new SqlDataAdapter(cmd);
                        dataAdapter.Fill(ds1);
                        GridView1.AutoGenerateColumns = false;
                        conn.Close();
                        if (ds1 != null)
                        {
                            if (ds1.Tables[0].Rows.Count != 0)
                            {
                                GridView1.DataSource = ds1;
                                GridView1.DataBind();
                            }
                            else
                            {
                                GridView1.DataSource = null;
                                GridView1.DataBind();
                            }
                        }
                    }
                }
                catch (Exception err)
                {
                    ErrorMessage.Text = err.Message.ToString();
                }
            }
        }

        protected void AddLanguage_Click(object sender, EventArgs e)
        {
            if (Ddlanguage.SelectedItem.Value != "0")
            {
                var translatorId = GetUserTranslatorId(User.Identity.GetUserId());

                if (!CheckIfLanguageExists(translatorId, Convert.ToInt32(Ddlanguage.SelectedItem.Value)))
                {
                    string query = string.Empty;
                    query += "INSERT INTO TranslatorAccess (TranslatorID, LanguageID, DocumentID, CreatedBy)  ";
                    query += "VALUES (@1, @2, @3, @4)";

                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        using (SqlCommand comm = new SqlCommand())
                        {
                            {
                                var withBlock = comm;
                                withBlock.Connection = conn;
                                withBlock.CommandType = CommandType.Text;
                                withBlock.CommandText = query;
                                withBlock.Parameters.AddWithValue("@1", translatorId);
                                withBlock.Parameters.AddWithValue("@2", Ddlanguage.SelectedItem.Value);
                                withBlock.Parameters.AddWithValue("@3", 1);
                                withBlock.Parameters.AddWithValue("@4", translatorId);
                            }
                            try
                            {
                                conn.Open();
                                comm.ExecuteNonQuery();
                                SuccessMessage.Text = "Language added successfully";
                                Response.Redirect(Request.RawUrl);
                            }
                            catch (SqlException ex)
                            {
                                ErrorMessage.Text = ex.Message.ToString();
                            }
                        }
                    }
                }
                else
                {
                    ErrorMessage.Text = "Selected Language have been already added!";
                }
            }
        }

        public int GetUserTranslatorId(string userId)
        {
            try
            {
                int translatrId = 0;
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SELECT TranslatorID FROM Translator WHERE UserID = @UserId", conn))
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Add("UserId", SqlDbType.VarChar).Value = userId;
                        translatrId = (Int32)cmd.ExecuteScalar();
                        conn.Close();
                    }
                }
                return translatrId;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool CheckIfLanguageExists(int tID, int lID)
        {
            bool exists = false;
            var SqlConnection2 = new SqlConnection();
            SqlConnection2.ConnectionString = _connectionString;
            SqlCommand cmd1 = new SqlCommand("Select * from TranslatorAccess where TranslatorID = @TID and LanguageID = @LID", SqlConnection2);
            cmd1.Parameters.AddWithValue("@TID", tID);
            cmd1.Parameters.AddWithValue("@LID", lID);
            cmd1.Connection.Open();
            SqlDataReader reader1 = cmd1.ExecuteReader(CommandBehavior.CloseConnection);
            exists = (reader1.HasRows);
            cmd1.Connection.Close();
            return exists;
        }
    }
}
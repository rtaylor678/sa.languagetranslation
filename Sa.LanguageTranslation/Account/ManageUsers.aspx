﻿<%@ Page Title="Manage Users" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="ManageUsers.aspx.cs" Inherits="Solomon.LanguageTranslation.Account.ManageUsers" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">

    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>

    <div>
        <asp:PlaceHolder runat="server" ID="PlaceHolder1" Visible="false" ViewStateMode="Disabled">
            <p class="text-success"><%: SuccessMessage %></p>
        </asp:PlaceHolder>
    </div>

    <div class="form-horizontal">
        <h4>Manage Users</h4>
        <hr />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />

        <div class="padding-top-30">
            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-12">
                        <asp:GridView ID="GridView1" DataKeyNames="Id" OnRowCommand="GridView1_RowCommand" CssClass="table table-striped table-bordered table-condensed" runat="server">
                            <%--PageSize="10" AllowPaging="true"--%>
                            <Columns>
                                <asp:BoundField DataField="UserName" HeaderText="UserName" />
                                <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                                <asp:BoundField DataField="LastName" HeaderText="Last Name" />
                                <asp:BoundField DataField="Email" HeaderText="Email" />
                                <asp:BoundField DataField="Role" HeaderText="User Role" />
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:Button runat="server" CommandName="ResetUser" Text="Reset Password" ID="ResetUser" CssClass="btn btn-default" ToolTip="Reset User Credentials" CommandArgument='<%# Eval("Id") %>'></asp:Button>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <!-- Bootstrap Modal Dialog -->
                        <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <asp:UpdatePanel ID="upModal" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title"><asp:Label ID="lblModalTitle" runat="server" Text=""></asp:Label></h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <asp:Label ID="lblModalBody" runat="server" Text=""></asp:Label>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" runat="server" onserverclick="reset_ServerClick" data-dismiss="modal" class="btn btn-primary" id="reset">Yes</button>
                                                <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

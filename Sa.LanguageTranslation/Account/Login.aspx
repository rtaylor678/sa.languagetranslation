﻿<%@ Page Title="Log in" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Solomon.LanguageTranslation.Account.Login" Async="true" %>

<%@ Register Src="~/Account/OpenAuthProviders.ascx" TagPrefix="uc" TagName="OpenAuthProviders" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">

    <div class="container">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header"><%: Title %></div>
            <div class="card-body">
                <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="FailureText" />
                    </p>
                </asp:PlaceHolder>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="UserName">UserName</asp:Label>
                    <asp:TextBox runat="server" ID="UserName" CssClass="form-control" TextMode="SingleLine" placeholder="Enter UserName" />
                    <asp:RequiredFieldValidator runat="server" Display="Dynamic" ControlToValidate="UserName" CssClass="text-danger" ErrorMessage="The UserName field is required." />
                </div>
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="Password">Password</asp:Label>
                    <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" placeholder="Password"/>
                    <asp:RequiredFieldValidator runat="server" Display="Dynamic" ControlToValidate="Password" CssClass="text-danger" ErrorMessage="The password field is required." />
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <asp:Label runat="server" AssociatedControlID="RememberMe" CssClass="form-check-label">
                            <asp:CheckBox runat="server" ID="RememberMe" CssClass="form-check-input" />
                            Remember Password</asp:Label>
                    </div>
                </div>
                <asp:Button runat="server" OnClick="LogIn" Text="Log in" CssClass="btn btn-primary btn-block" />
                <div class="text-center"></div>
            </div>
        </div>
    </div>

</asp:Content>

﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using Solomon.LanguageTranslation.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;
using System.Security.Claims;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using System.Web.Security;

namespace Solomon.LanguageTranslation.Account
{
    public partial class Register : Page
    {
        #region private variables
        string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString.ToString();
        #endregion

        protected void CreateUser_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
            var user = new ApplicationUser() { UserName = UserName.Text, Email = Email.Text };
            //string password = Membership.GeneratePassword(12, 1);
            IdentityResult result = manager.Create(user, Password.Text);
            if (result.Succeeded)
            {
                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                string code = manager.GenerateEmailConfirmationToken(user.Id);
                string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);
                string message = "Below are your temporary Login Credentials for Solomon Language Translator application.<br /><br /><b> UserName: " + UserName.Text + "</b><br /><b> Password: " + Password.Text + "</b><br /> <br /><br /> Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>. Make sure you change your login password after the first login.";
                manager.SendEmail(user.Id, "Confirm your account", message);

                //Assign Role to user Here   
                manager.AddToRole(user.Id, UserRole.SelectedValue);

                //signInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                //IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);

                var SqlCon1 = new SqlConnection();
                SqlCon1.ConnectionString = _connectionString;
                SqlCommand cmd = new SqlCommand("sp_UserInsert", SqlCon1);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("Username", user.UserName);
                cmd.Parameters.AddWithValue("UserID", user.Id);
                cmd.Parameters.AddWithValue("FirstName", FirstName.Text);
                cmd.Parameters.AddWithValue("LastName", LastName.Text);
                cmd.Parameters.AddWithValue("ForceReset", 1);
                SqlCon1.Open();
                int k = cmd.ExecuteNonQuery();
                if (k != 0)
                {
                    ErrorMessage.Text = "";
                    SuccessMessage.Text = "User Created Succesfully! An email will be sent to the user to activate their account.";
                    ClearForm();
                }
                SqlCon1.Close();
            }
            else
            {
                SuccessMessage.Text = "";
                ErrorMessage.Text = result.Errors.FirstOrDefault();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!((HttpContext.Current.User != null) && HttpContext.Current.User.Identity.IsAuthenticated))
            {
                Response.Redirect("/Account/Login");
            }
            var manager = HttpContext.Current.GetOwinContext();

            var roleStore = new RoleStore<IdentityRole>(manager.Get<ApplicationDbContext>());
            var roleMngr = new RoleManager<IdentityRole>(roleStore);

            var roles = roleMngr.Roles.Where(u => !u.Name.Contains("SuperAdmin")).OrderBy(r => r.Name).ToList().Select(rr => new ListItem { Value = rr.Name, Text = rr.Name }).ToArray();

            //var roles = ((ClaimsIdentity)User.Identity).Claims
            //    .Where(c => c.Type == ClaimTypes.Role)
            //    .Select(rr => new ListItem { Value = rr.Value, Text = rr.Value }).ToArray();

            //var list = ApplicationDbContext.Roles.Where(u => !u.Name.Contains("SuperAdmin")).OrderBy(r => r.Name).ToList().Select(rr => new ListItem { Value = rr.Name, Text = rr.Name }).ToArray();
            UserRole.Items.AddRange(roles);
        }

        private void ClearForm()
        {
            Email.Text = "";
            UserName.Text = "";
            FirstName.Text = "";
            LastName.Text = "";
        }
    }
}
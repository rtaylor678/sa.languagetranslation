﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using Solomon.LanguageTranslation.Models;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web.Configuration;

[assembly: OwinStartupAttribute(typeof(Solomon.LanguageTranslation.Startup))]
namespace Solomon.LanguageTranslation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesandUsers();
        }

        // Create roles
        private void createRolesandUsers()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));


            // On startup create SuperAdmin role 
            if (!roleManager.RoleExists("SuperAdmin"))
            {
                var role = new IdentityRole();
                role.Name = "SuperAdmin";
                roleManager.Create(role);

                //Here we create a site admin user		
                var user = new ApplicationUser();
                user.UserName = WebConfigurationManager.AppSettings["AdminUserName"];
                user.Email = WebConfigurationManager.AppSettings["AdminEmail"];
                user.EmailConfirmed = true;
                string userPWD = WebConfigurationManager.AppSettings["AdminPassword"];
                var chkUser = UserManager.Create(user, userPWD);

                //Add default User to Role SuperAdmin
                if (chkUser.Succeeded)
                {
                    var result1 = UserManager.AddToRole(user.Id, "SuperAdmin");
                    CreateTranslatorForSuperAdmin(user);
                    CreatDefaultSystemLanguage();
                }
            }

            // create Admin role 
            if (!roleManager.RoleExists("Admin"))
            {
                var role = new IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);
            }

            // create Translator role 
            if (!roleManager.RoleExists("Translator"))
            {
                var role = new IdentityRole();
                role.Name = "Translator";
                roleManager.Create(role);
            }
        }

        private void CreateTranslatorForSuperAdmin(ApplicationUser user)
        {
            var SqlCon1 = new SqlConnection();
            SqlCon1.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString.ToString();
            SqlCommand cmd = new SqlCommand("sp_UserInsert", SqlCon1);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Username", user.UserName);
            cmd.Parameters.AddWithValue("UserID", user.Id);
            cmd.Parameters.AddWithValue("FirstName", user.UserName);
            cmd.Parameters.AddWithValue("LastName", "");
            cmd.Parameters.AddWithValue("ForceReset", 0);
            SqlCon1.Open();
            cmd.ExecuteNonQuery();
            SqlCon1.Close();
        }

        private void CreatDefaultSystemLanguage()
        {
            var en = CultureInfo.GetCultures(CultureTypes.AllCultures)
                                .FirstOrDefault(r => r.EnglishName == "English");
            string query = "INSERT INTO Languages (LanguageName, LanguageAbbr) VALUES (@1, @2)";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString.ToString()))
            {
                using (SqlCommand comm = new SqlCommand())
                {
                    {
                        var withBlock = comm;
                        withBlock.Connection = conn;
                        withBlock.CommandType = CommandType.Text;
                        withBlock.CommandText = query;
                        withBlock.Parameters.AddWithValue("@1", en.DisplayName);
                        withBlock.Parameters.AddWithValue("@2", en.Name);
                    }
                    try
                    {
                        conn.Open();
                        comm.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        throw;
                    }
                }
            }
        }
    }
}
